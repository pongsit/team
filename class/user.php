<?php
namespace pongsit\team;


class user extends \pongsit\model\model{
	
	public function __construct() 
    {
    	parent::__construct();
    	$this->table = str_replace('pongsit\\', '', __NAMESPACE__).'_'.$this->table;
    }
    
	function get_all2($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$order_by='user.id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user.active = 1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_all_allow($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$order_by='role_id DESC, user.id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow=1 and admin_allow=1 and user.active = 1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_all_allow_count($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$query = "SELECT count(user_id) as c FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow=1 and admin_allow=1 and user.active = 1";
		$outputs = $this->db->query_array0($query);
		return $outputs['c'];
	}
	function get_all_admin($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow=1 and role_id=1 and admin_allow=1 and user.active = 1 ";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_all_no_admin($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow=1 and (role_id is null or role_id = 0) and admin_allow=1 and user.active = 1 ";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_user_allow0_admin_allow1($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$order_by='user.id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow = 0 and admin_allow = 1 and user.active = 1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_all_user_allow1_admin_allow0($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow = 1 and admin_allow=0 and user.active = 1";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_all_user_allow0_admin_allow1($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow = 0 and admin_allow = 1 and user.active = 1";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_user_allow1_admin_allow0($inputs){
		$team_id='';
		if(!empty($inputs['team_id'])){ 
			$team_id=$inputs['team_id']; 
		}else{
			return false;
		}
		$order_by='user.id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT user_id FROM `".$this->table."` inner join user on user.id=team_user.user_id WHERE team_id='$team_id' and user_allow = 1 and admin_allow=0 and user.active = 1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function insert_once2($inputs){
		if(empty($inputs['user_id'])){ return false; }
		if(empty($inputs['team_id'])){ return false; }
		$query = "SELECT * FROM `".$this->table."` WHERE user_id='".$inputs['user_id']."' and team_id='".$inputs['team_id']."';";
		$infos = $this->db->query_array0($query);
		if(empty($infos)){
			return $this->db->insert('team_user',$inputs);
		}else{
			return false;
		}
	}

	function get_info($team_user_id){
		$query = "SELECT * FROM `".$this->table."` WHERE id = '".$team_user_id."';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs)){
			return $outputs;
		}
	}

	function get_info2($team_id,$user_id){
		$query = "SELECT * FROM `".$this->table."` WHERE team_id = '".$team_id."' and user_id = '".$user_id."';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs)){
			return $outputs;
		}
	}

	function update($inputs,$where=""){
		if(empty($where) && !empty($inputs['id'])){
			return $this->db->update($this->table,$inputs,'id='.$inputs['id']);
		}
		if(!empty($where)){
			return $this->db->update($this->table,$inputs,$where);
		}
	}

	function is_member($team_id,$user_id){
		$query = 'SELECT count(team_id) as c
					FROM '.$this->table.'
			  	   WHERE user_id='.$user_id.' and team_id='.$team_id.'  
			  	;';
		$outputs = $this->db->query_array0($query);
		if($outputs['c'] > 0){
			return true;
		}else{
			return false;
		}	
	}
}