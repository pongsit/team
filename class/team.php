<?php
namespace pongsit\team;


class team extends \pongsit\model\model{
	
	public function __construct() 
    {
    	parent::__construct();
    }
    
    function search_all_active($inputs=array()){
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$search='';
		if(!empty($inputs['search'])){ 
			$search = ' and  (
				name like "%'.$inputs['search'].'%" or 
				name_show like "%'.$inputs['search'].'%" 
			)'; 
		}
		$query = "SELECT * FROM `".$this->table."` where active=1 and allow_search = 1 $search ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
    
    function get_id($team_name){
		$query = "SELECT id FROM ".$this->table." WHERE name='$team_name';";
		$results = $this->db->query_array0($query);
		if(empty($results)){
			return false;
		}else{
			return $results['id'];
		}
		
	}
	
	function get_name($team_id){
		$query = "SELECT name FROM ".$this->table." WHERE id='$team_id';";
		$results = $this->db->query_array($query);
		if(empty($results[0]['name'])){
			return false;
		}else{
			return $results[0]['name'];
		}
	}
	function get_name_show($team_id){
		$query = "SELECT name_show FROM ".$this->table." WHERE id='$team_id';";
		$results = $this->db->query_array($query);
		if(empty($results[0]['name_show'])){
			return '';
		}else{
			return $results[0]['name_show'];
		}
	}
	function get_name_show_from_name($name){
		$team_id = $this->get_id($name);
		$name_show = $this->get_name_show($team_id);
		return $name_show;
	}
	function get_power($team_id){
		$query = "SELECT power FROM ".$this->table." WHERE id='$team_id' and team.active = 1 ;";
		$results = $this->db->query_array($query);
		if(empty($results[0]['power'])){
			return 0;
		}else{
			return $results[0]['power'];
		}
	}
	
	// ---- join
    
	function check($role_name,$team_id){
		if(empty($_SESSION['user']['id']) || empty($team_id)){
			return false;
		}
		$query = 'SELECT count(role.id) as c 
					    FROM team_user 
				  INNER JOIN role ON team_user.role_id = role.id 
					   WHERE user_id='.$_SESSION['user']['id'].'
					   		 AND team_user.team_id = '.$team_id.' 
					   		 AND role.active = 1 
					    	 AND role.name="'.$role_name.'" 
					;';
		$teams = $this->db->query_array0($query);
		if(empty($teams['c'])){
			return false;
		}
		if($teams['c']>0){
			return true;
		}else{
			return false;
		}
	}
	function show(){
		$query = 'SELECT team.name
					FROM team_user 
			  INNER JOIN team ON team_user.team_id = team.id 
				   WHERE team_user.user_id='.$_SESSION['user']['id'].'
				;';
		$teams = $this->db->query_array($query);
		return $teams;	
	}

	function get_all_name_for($user_id){
		$query = 'SELECT team.name as team_name
					FROM team_user
			  INNER JOIN team ON team_user.team_id = team.id 
			  	   WHERE team_user.user_id='.$user_id.' and team.active = 1 
			  	ORDER BY team_user.team_id ASC
			  ;';
		$teams = $this->db->query_array($query);
		$outputs=array();
		foreach($teams as $key=>$values){
			$outputs[] = $values['team_name'];
		}
		return $outputs;	
	}
	function get_all_team_info_for($user_id){
		$query = 'SELECT *, team.name as name, team.id as id
					FROM team_user
			  INNER JOIN team ON team_user.team_id = team.id 
			  	   WHERE team_user.user_id='.$user_id.' and team.active = 1 
			  	ORDER BY team_user.team_id ASC
			  ;';
		$teams = $this->db->query_array($query);
		$outputs=array();
		foreach($teams as $key=>$values){
			$outputs[] = $values;
		}
		return $outputs;	
	}
	function get_all_name_show_for($user_id){
		$query = 'SELECT team.name_show as team_name_show
					FROM team_user
			  INNER JOIN team ON team_user.team_id = team.id 
			  	   WHERE team_user.user_id='.$user_id.' and team.active = 1 
			  	ORDER BY team_user.team_id ASC
			  ;';
		$teams = $this->db->query_array($query);
		$outputs=array();
		foreach($teams as $key=>$values){
			$outputs[] = $values['team_name_show'];
		}
		return $outputs;	
	}
	function get_all_id_for($user_id){
		$query = 'SELECT team_id
					FROM team_user
			  	   WHERE user_id='.$user_id.' 
			  	ORDER BY team_id ASC
			  	;';
		$teams = $this->db->query_array($query);
		$outputs=array();
		foreach($teams as $key=>$values){
			$outputs[] = $values['team_id'];
		}
		return $outputs;	
	}
	function get_all_team_id(){
		$all_ids = $this->get_all();
		$outputs = array();
		foreach($all_ids as $key=>$values){
			$outputs[] = $values['id'];
		}
		return $outputs;
	}
	function get_max_power($user_id){
		if($user_id==1){
			return 1000;
		}else{
			$query = 'SELECT max(team.power) as the_power 
						FROM team 
				  INNER JOIN team_user
						  ON team.id=team_user.team_id and team.active = 1 
					   WHERE user_id='.$user_id.'
					;';
			$infos = $this->db->query_array0($query);
			return $infos['the_power'];
		}
	}
	function get_all_above($user_id){
		$max_power = $this->get_max_power($user_id);
		$query = 'SELECT *  
					FROM team 
			  	   WHERE power > '.$max_power.'
			  	ORDER BY power DESC
			  	;';
		$infos = $this->db->query_array($query);
		return $infos;
	}

}