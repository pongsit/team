<?php
	
require_once("../system/init.php");
$team = new \pongsit\team\team();
$team_user = new \pongsit\team\user();

$results['done'] = 0;

if(empty($_POST['type'])){
	exit();
}

switch($_POST['type']){
	case 'image-upload':
		$file = new \pongsit\file\file();

		// error_log(print_r($_POST,true));
		// error_log(print_r($_FILES,true));

		/*
		$_POST = array(
			[path] => ../system/img/profile/
			[team_id] => 1
		)

		$_FILES = Array
		(
		    [file] => Array
		        (
		            [name] => Screen Shot 2563-08-20 at 23.58.43.png
		            [type] => image/png
		            [tmp_name] => /Applications/MAMP/tmp/php/php7ICYwh
		            [error] => 0
		            [size] => 14670
		        )

		)


		*/

		$results = array();
		if(empty($_FILES)){
			$results['error'] = 'Error: No file found!';
		}else{
			$file_type = $_FILES['file']['type'];

			$ext = '';
			if($file_type == 'image/jpeg' || $file_type == 'image/png'){
				if(!empty($_POST['team_id'])){
					$team_id = $_POST['team_id'];
				}else{
					$results['error'] = 'Error: No team_id';
					exit();
				}
				$path_infos = pathinfo($file_type);
				// $ext = $path_infos['basename'];
				switch($file_type){
					case 'image/jpeg': $ext='jpg'; break;
					case 'image/png': $ext='png'; break;
				}
				$image_folder = $path_to_app.'system/img/team/'.$team_id;
				if(!file_exists($image_folder)){
					mkdir($image_folder,0755,true);
				}
				$image_location = $path_to_app.'system/img/team/'.$team_id.'/profile';
				$file->delete_all_with_file_name($image_location);
				rename($_FILES['file']['tmp_name'], $image_location);
				chmod($image_location,0644);
				$results['path'] = $image_location;
			}else{
				$results['error'] = '.jpg or .png file only!';
			}
		}
		break;
	case 'join_request':
		$args = array();
		$args['team_id'] = $_POST['team_id'];
		$args['user_id'] = $_POST['user_id'];
		$args['user_allow'] = 1;
		if(!empty($team_user->insert_once2($args))){
			$results['done'] = 1;
			$results['team_id'] = $args['team_id'];
		}
		break;
	case 'user_allow_request':
		$args = array();
		if(empty($_POST['team_id']) || empty($_POST['team_user_id'])){
			header('Content-Type: application/json');
			echo json_encode($results);
			exit();
		}
		$args['id'] = $_POST['team_user_id'];
		$args['user_allow'] = 1;
		if(!empty($team_user->update($args))){
			$results['done'] = 1;
			$results['team_user_id'] = $args['team_user_id'];
			$results['team_id'] = $_POST['team_id'];
			$team_user_infos = $team_user->get_info($_POST['team_user_id']);
			if($team_user_infos['admin_allow'] && $team_user_infos['user_allow']){
				$results['user_status'] = '<em>(เป็นสมาชิก)</em>';
			}
			if(!$team_user_infos['admin_allow']){
				$results['user_status'] = '<em>(รออนุมัติ)</em>';
			}
		}
		break;
	case 'admin_cancel_user':
		$team_user_infos = $team_user->get_info($_POST['team_user_id']);
		if($team->check('admin',$team_user_infos['team_id'])){
			if(!empty($_POST['team_user_id'])){
				$args = array();
				$args['id'] = $_POST['team_user_id'];
				$args['admin_allow'] = 0;
				if(!empty($team_user->update($args))){
					$results['done'] = 1;
					$results['team_user_id'] = $_POST['team_user_id'];
				}
			}
		}
		break;
	case 'admin_allow_user':
		$team_user_infos = $team_user->get_info($_POST['team_user_id']);
		if($team->check('admin',$team_user_infos['team_id'])){
			if(!empty($_POST['team_user_id'])){
				$args = array();
				$args['id'] = $_POST['team_user_id'];
				$args['admin_allow'] = 1;
				if(!empty($team_user->update($args))){
					$results['done'] = 1;
					$results['team_user_id'] = $_POST['team_user_id'];
				}
			}
		}
		break;
	case 'admin_add_admin':
		$team_user_infos = $team_user->get_info($_POST['team_user_id']);
		if($team->check('admin',$team_user_infos['team_id'])){
			if(!empty($_POST['team_user_id'])){
				$args = array();
				$args['id'] = $_POST['team_user_id'];
				$args['role_id'] = 1;
				if(!empty($team_user->update($args))){
					$results['done'] = 1;
					$results['team_user_id'] = $_POST['team_user_id'];
				}
			}
		}
		break;
	case 'admin_to_no_admin':
		$team_user_infos = $team_user->get_info($_POST['team_user_id']);
		if($team->check('admin',$team_user_infos['team_id'])){
			if(!empty($_POST['team_user_id'])){
				$args = array();
				$args['id'] = $_POST['team_user_id'];
				$args['role_id'] = 0;
				if(!empty($team_user->update($args))){
					$results['done'] = 1;
					$results['team_user_id'] = $_POST['team_user_id'];
				}
			}
		}
		break;
	case 'admin_remove_user':
		$team_user_infos = $team_user->get_info($_POST['team_user_id']);
		if($team->check('admin',$team_user_infos['team_id'])){
			if(!empty($_POST['team_user_id'])){
				$args = array();
				$args['id'] = $_POST['team_user_id'];
				$args['admin_allow'] = 0;
				if(!empty($team_user->update($args))){
					$results['done'] = 1;
					$results['team_user_id'] = $_POST['team_user_id'];
				}
			}
		}
		break;
}

if(empty($results)){
	exit();
}

header('Content-Type: application/json');
echo json_encode($results);