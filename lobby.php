<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();
$user = new \pongsit\user\user();
$team_user = new \pongsit\team\user();
$role = new \pongsit\role\role();

if(empty($_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$id = $_GET['id'];
$team_id = $_GET['id'];

if(!($team->check('admin',$team_id) || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$team_infos = $team->get_info($team_id);

$list = '';

$vs = array();
$vs['add_member'] = '';
$vs['card_name'] = 'ผู้บริหาร';
$team_show1s = $team_user->get_all_admin(array('team_id'=>$team_id));
// Array ( [0] => Array ( [user_id] => 1 ) ) 
// print_r($team_show1s);
// exit();
$vs['list'] = '';
if(!empty($team_show1s)){
	$vs['list'] .= '
<div class="d-flex team_id'.$team_id.'-member">';
	$team_user_infos = $team_user->get_info2($team_id,$team_infos['super_admin']);
	$vs['list'] .= '<div><div style="position:relative;">';
	$vs['list'] .= $view->block('list-no-admin', array('id'=>$team_id,$team_infos['super_admin'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
	if($team_user_infos['role_id']==1){
		$vs['list'] .= '<i style="position:absolute;bottom:5px;right:8px;font-size:0.6em;color:yellow;" class="fas fa-crown"></i>';
	}
	$vs['list'] .= '</div></div>';
	foreach($team_show1s as $_k=>$_vs){
		if($team_infos['super_admin'] == $_vs['user_id']){ continue; }
		$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
		$vs['list'] .= '<div>';
		if($team->check('admin',$team_id)){
			$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
			$this_user_infos = $user->get_info($_vs['user_id']);
			$vs['list'] .= '<div><div style="position:relative;">';
			$vs['list'] .= $view->block('list', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
			if($team_user_infos['role_id']==1){
				$vs['list'] .= '<i style="position:absolute;bottom:5px;right:8px;font-size:0.6em;" class="fas fa-crown"></i>';	
			}
			$vs['list'] .= '</div></div>';
			$vs['list'] .= $view->block('list-admin', array(
						'team_user_id'=>$team_user_infos['id'],
						'id'=>$_vs['user_id'],
						'name'=>$this_user_infos['name']
					));
		}else{
			$vs['list'] .= $view->block('list-no-admin', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
		}
		$vs['list'] .= '</div>';
	}
	$vs['list'] .= '</div>';
}
$vs['team_id'] = $team_id;
$list .= $view->block('lobby',$vs);

$vs = array();
$vs['add_member'] = '';
$vs['card_name'] = 'สมาชิก';
$team_show1s = $team_user->get_all_no_admin(array('team_id'=>$team_id));
// Array ( [0] => Array ( [user_id] => 1 ) ) 
// print_r($team_show1s);
// exit();
$vs['list'] = '';
if(!empty($team_show1s)){
	$vs['list'] .= '
<div class="d-flex team_id'.$team_id.'-member">';
	foreach($team_show1s as $_k=>$_vs){
		$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
		$vs['list'] .= '<div style="position:relative;">';
		// $vs['list'] .= $view->block('list', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
		if($team->check('admin',$team_id)){
			$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
			$this_user_infos = $user->get_info($_vs['user_id']);
			$vs['list'] .= $view->block('list', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
			$vs['list'] .= $view->block('list-member', array(
						'team_user_id'=>$team_user_infos['id'],
						'id'=>$_vs['user_id'],
						'name'=>$this_user_infos['name']
					));
		}else{
			$vs['list'] .= $view->block('list-no-admin', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
		}
		$vs['list'] .= '</div>';
	}
	$vs['list'] .= '</div>';
}
$vs['team_id'] = $team_id;
$list .= $view->block('lobby',$vs);

$vs = array();
$vs['add_member'] = '';
$vs['card_name'] = 'รออนุมัติ';
$vs['list'] = '';
$team_show2s = $team_user->get_all_user_allow1_admin_allow0(array('team_id'=>$team_id));
if(!empty($team_show2s)){
	$vs['list'] .= '<div>';
	foreach($team_show2s  as $_k=>$_vs){
		if($team->check('admin',$team_id)){
			$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
			$this_user_infos = $user->get_info($_vs['user_id']);
			$vs['list'] .= $view->block('list', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
			$vs['list'] .= $view->block('list-wait', array(
						'team_user_id'=>$team_user_infos['id'],
						'id'=>$_vs['user_id'],
						'name'=>$this_user_infos['name']
					));
		}else{
			$vs['list'] .= $view->block('list-no-admin', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
		}
	}
	$vs['list'] .= '</div>';
}
$vs['team_id'] = $team_id;
if(!empty($vs['list'])){
	$list .= $view->block('lobby',$vs);
}

$vs = array();
$vs['add_member'] = '';
$vs['card_name'] = 'รอตกลงเข้าร่วม';
$vs['list'] = '';
$team_show2s = $team_user->get_all_user_allow0_admin_allow1(array('team_id'=>$team_id));
if(!empty($team_show2s)){
	$vs['list'] .= '<div>';
	foreach($team_show2s  as $_k=>$_vs){
		if($team->check('admin',$team_id)){
			$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
			$this_user_infos = $user->get_info($_vs['user_id']);
			$vs['list'] .= $view->block('list', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
			$vs['list'] .= $view->block('list-user-allow', array(
						'team_user_id'=>$team_user_infos['id'],
						'id'=>$_vs['user_id'],
						'name'=>$this_user_infos['name']
					));
		}else{
			$vs['list'] .= $view->block('list-no-admin', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
		}
	}
	$vs['list'] .= '</div>';
}
$vs['team_id'] = $team_id;
if(!empty($vs['list'])){
	$list .= $view->block('lobby',$vs);
}

$variables['list'] = $list;

$variables['page-name'] = 'ทีม';
$variables['menu-search'] = '
<div class="mt-2 mr-sm-2 flex-fill">
<form action="'.$path_to_core.'team/search.php" method="get">
	<div class="d-flex">
		<div class="flex-fill mr-2"><input class="bg-blue-light border border-secondary text-white form-control mr-sm-2" type="search" name="search" placeholder="ค้นหา"></div>
		<div class="ml-auto"><button  style="line-height:36px;" class="btn btn-outline-secondary" type="submit">ค้นหา</button></div>
	</div>
</form>
</div>';
$variables['team_image'] = '';
if(file_exists('<img class="mr-2" style="width:50px;" src="'.$path_to_app.'system/img/team/'.$team_id.'/profile">')){
	$variables['team_image'] = '<img class="mr-2" style="width:50px;" src="'.$path_to_app.'system/img/team/'.$team_id.'/profile">';
}
$variables['team_name'] = $team_infos['name_show'];
$variables['team_detail'] = '';
if(!empty($team_infos['detail'])){
	$variables['team_detail'] = '<em>'.$team_infos['detail'].'</em>';
}
$variables['add_friend'] = '
<button id="get_link" href="javascript" style="color:grey;position:relative;bottom:3px;" class="btn btn-light ml-2">
<span class="d-md-none"><i class="fas fa-copy" style="position:relative;top:3px;"></i></span>
<span class="d-none d-md-block"><i class="fas fa-copy" style="position:relative;top:3px;"></i> Copy Link</span>
</button>';
$variables['join_url'] = $urls['host'].'/join.php?team_id='.$team_id;

echo $view->create($variables);
