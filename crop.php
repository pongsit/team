<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}else{
	$id = +$_GET['id'];
	$team_id = $id;
}

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$profile = new \pongsit\profile\profile();
$line = new \pongsit\line\line();

// ผู้ที่เข้าชมได้
if(!($team->check('admin',$team_id) || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$variables['head'] = '
<link rel="stylesheet" href="'.$path_to_core.'croppie/css/croppie.css" />
<script src="'.$path_to_core.'croppie/js/croppie.js"></script>';

$variables['team_id'] = $team_id;
$variables['image_version'] = rand(1000,9999);

echo $view->create($variables);

