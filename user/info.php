<?php
	
require_once("../../system/init.php");

$team = new \pongsit\team\team();
$user = new \pongsit\user\user();
$team_user = new \pongsit\team\user();

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$id = $_GET['id'];

$team_user_infos = $team_user->get_info($id);
$team_id = $team_user_infos['team_id'];
$user_id = $team_user_infos['user_id'];

if(!($team->check('admin',$team_id) || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}


echo $view->create($variables);
