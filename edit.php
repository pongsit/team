<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();
$user = new \pongsit\user\user();

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$id = $_GET['id'];
$team_id = $id;

if(!($team->check('admin',$team_id))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$team_infos = $team->get_info($id);
// Array ( [id] => 1 [name] => admin [power] => 100 [name_show] => ผู้บริหาร ) 
// print_r($team_infos);
// exit();

$info_show = '';
$variables['active_show'] = '';
$variables['allow_search_show'] = '';
if(!empty($team_infos)){
	$allow_search = '';
	$not_allow_search = '';
	if($team_infos['allow_search']=='1'){
		$allow_search='checked';
	}else{
		$not_allow_search='checked';
	}
	$variables['allow_search_show']='
<div class="form-check form-check-inline">
	<input class="form-check-input" type="radio" name="allow_search" id="allow_search1" value="1" '.$allow_search.'>
	<label class="form-check-label" for="allow_search1">ค้นหาทีมได้</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="allow_search" id="allow_search2" value="0" '.$not_allow_search.'>
  <label class="form-check-label" for="allow_search2">ไม่สามารถค้นหาทีมนี้ได้</label>
</div>
';
	$active = '';
	$not_active = '';
	if($team_infos['active']=='1'){
		$active='checked';
	}else{
		$not_active='checked';
	}
	$variables['active_show']='
<div class="form-check form-check-inline">
	<input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1" '.$active.'>
	<label class="form-check-label" for="inlineRadio1">ปกติ</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0" '.$not_active.'>
  <label class="form-check-label" for="inlineRadio2">ปิดทีม</label>
</div>
<script>
	$(function(){
		$("body").on("click tap","#inlineRadio2",function(){
			alert("คุณกำลังจะยกเลิกทีมนี้ หากคุณส่งข้อมูล");
		});
	});
</script>
';
	$variables['name_show'] = $team_infos['name_show'];
	$variables['detail'] = $team_infos['detail'];
}

$variables['id'] = $id;
$variables['notification']='';
$variables['page-name'] = 'แก้ข้อมูลทีม';

if(!empty($_POST)){
// 	Array ( [power] => 100 [name_show] => ผู้บริหาร [id] => 1 ) 
// 	print_r($_POST);
// 	exit();
	foreach($_POST as $key=>$value){
		if(!is_numeric($value)){
			if(empty($value)){unset($_POST[$key]);}
		}
	}
	if($_POST['power']>100){ $_POST['power']=100; }
	if($team->update($_POST)){
		header('Location:'.$path_to_core.'team/index.php?user_id='.$_SESSION['user']['id']);
	}
}

$variables['image_version'] = rand(1000,9999);
$variables['upload-image'] = $view->ajax('upload-image',array(
'url'=>'ajax.php',
'css'=>'btn btn-outline-primary btn-sm',
'json'=>'{team_id:'.$team_id.'}',
'call_back' => 'window.location = "'.$path_to_core.'team/crop.php?id='.$team_id.'"'
));
$variables['team_image']=$path_to_core.'system/img/profile/male.png';
if(file_exists($path_to_app.'system/img/team/'.$team_id.'/profile')){
	$variables['team_image']=$path_to_app.'system/img/team/'.$team_id.'/profile';
}

$variables['info-list']=$info_show;
echo $view->create($variables);
