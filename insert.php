<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();
$user = new \pongsit\user\user();
$time = new \pongsit\time\time();
$team_user = new \pongsit\team\user();

$variables['page-name'] = 'เพิ่มทีม';

if(!empty($_POST)){
	$inserts = array();
	$inserts['name'] = $auth->safe_encrypt($time->now(), '');
	$inserts['name_show'] = $_POST['name_show'];
	$inserts['super_admin'] = $_SESSION['user']['id'];
	if(mb_strlen($inserts['name_show']) < 5){
		$variables['notification'] = $view->block('alert',array('type'=>'danger','message'=>'ชื่อทีมต้องมีความยาวตั้งแต่ 5 อักษรขึ้นไปครับ','css'=>'col'));
		echo $view->create($variables);
		exit();
	}
	
	$team_id = $team->insert($inserts);
	if(!empty($team_id)){
		$inserts = array();
		$inserts['role_id'] = 1;
		$inserts['team_id'] = $team_id;
		$inserts['user_id'] = $_SESSION['user']['id'];
		$inserts['user_allow'] = 1;
		$inserts['admin_allow'] = 1;
		$team_user_id = $team_user->insert_once2($inserts);
		if(!empty($team_user_id)){
			header('Location:'.$path_to_core.'team/index.php?user_id='.$_SESSION['user']['id']);
		}else{
			$variables['notification'] = $view->block('alert',array('type'=>'danger','message'=>'มีบางอย่างผิดพลาด กรุณาลองใหม่ภายหลังครับ','css'=>'col'));
			echo $view->create($variables);
			exit();
		}
	}else{
		$variables['notification'] = $view->block('alert',array('type'=>'danger','message'=>'มีบางอย่างผิดพลาด กรุณาลองใหม่ภายหลังครับ','css'=>'col'));
		echo $view->create($variables);
		exit();
	}

}

echo $view->create($variables);
