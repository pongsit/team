<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();
$team_user = new \pongsit\team\user();

$variables['menu-search'] = '
<div class="mt-2 mr-sm-2 flex-fill">
<form method="get">
	<div class="d-flex">
		<div class="flex-fill mr-2"><input class="form-control mr-sm-2 border border-secondary text-white" type="search" name="search" placeholder="ค้นหา"></div>
		<div class="ml-auto"><button class="btn btn-outline-secondary search-submit" type="submit">ค้นหา</button></div>
	</div>
</form>
</div>';

if(empty($_GET['search'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
// if(mb_strlen($_GET['search']) < 5){
// 	$variables['notification'] = $view->block('alert',array('type'=>'danger','message'=>'คำค้นหาต้องมีความยาวไม่น้อยกว่า 5 ตัวอักษรครับ','css'=>'col'));
// 	echo $view->create($variables,true);
// 	exit();
// }

$search = $_GET['search'];
$args = array();
$args['search'] = $search;
$args['limit'] = 10;

$team_infos = $team->search_all_active($args);

$list = '';
$limit = 10;
if(!empty($team_infos)){
	foreach($team_infos as $k=>$vs){
		$vs['team_name'] = ucfirst($vs['name']);
		if(!empty($vs['name_show'])){
			$vs['team_name'] = $vs['name_show'];
		}
		$team_id = $vs['id'];
		$vs['team_id'] = $team_id;
		$vs['active_show']='';
		if($vs['active']!=1){$vs['active_show']='สถานะ: <span class="text-danger">ระงับการใช้งาน</span>';}
		$vs['admin_edit'] = '';
		if($team->check('admin',$team_id)){
			$vs['admin_edit'] = '<a href="edit.php?id='.$team_id.'">แก้ไข</a>';
		}
		$vs['team_image']='';
		if(file_exists($path_to_app.'system/img/team/'.$team_id.'/profile')){
			$vs['team_image']='<img class="mr-2" style="width:50px;" src="'.$path_to_app.'system/img/team/'.$team_id.'/profile">';
		}
		if(!empty($vs['detail'])){
			$vs['detail'] = $vs['detail'];
		}
		$team_user_infos = $team_user->get_info2($team_id,$_SESSION['user']['id']);
		$vs['join_menu'] = '';
		$vs['user_status'] = '';
		if(!empty($team_user_infos)){
			if($team_user_infos['admin_allow'] && $team_user_infos['user_allow']){
				$vs['user_status'] = '<em>(เป็นสมาชิก)</em>';
			}
			if(!$team_user_infos['admin_allow']){
				$vs['user_status'] = '<em>(รออนุมัติ)</em>';
			}
			if(!$team_user_infos['user_allow']){
				$vs['user_status'] = '<a data-team_user_id="'.$team_user_infos['id'].'" data-team_id="'.$team_id.'" class="btn btn-info user_allow_request" href="javascript:;">ยืนยันเข้าร่วม</a>';
			}
		}else{
			switch($vs['join_method']){
				default:
					$vs['join_menu'] = '<a data-team_id="'.$team_id.'" class="btn btn-info join_request" href="javascript:;">ขอเข้าร่วม</a>'; 
			}
		}
		$list .= $view->block('search',$vs);
	}
}

$variables['list'] = $list;
$variables['page-name'] = 'ค้นหาทีม';
$variables['user_id'] = $_SESSION['user']['id'];
echo $view->create($variables);
