<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();
$user = new \pongsit\user\user();
$team_user = new \pongsit\team\user();
$role = new \pongsit\role\role();

if(!($role->check('admin') || $_SESSION['user']['id'] == 1)){
	if(empty($_GET['user_id'])){
		$view = new \pongsit\view\view('warning');
		echo $view->create();
		exit();
	}
	$user_id = $_GET['user_id'];
}

if(!($user_id == $_SESSION['user']['id'] || $role->check('admin') || $_SESSION['user']['id']==1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

if(empty($user_id)){
	$team_infos = $team->get_all();
}else{
	$team_infos = $team->get_all_team_info_for($user_id);
}

$list = '';
$limit = 10;
if(!empty($team_infos)){
	foreach($team_infos as $k=>$vs){
		$vs['card_name'] = ucfirst($vs['name']);
		if(!empty($vs['name_show'])){
			$vs['card_name'] = $vs['name_show'];
		}
		$team_id = $vs['id'];
		$team_show1s = $team_user->get_all_admin(array('team_id'=>$team_id));
		$member_count = $team_user->get_all_allow_count(array('team_id'=>$team_id));
// 		Array ( [0] => Array ( [user_id] => 1 ) ) 
// 		print_r($team_show1s);
// 		exit();
		$_list = '';
		if(!empty($team_show1s)){
			$manage_button = '';
			if($team->check('admin',$team_id)){
				$manage_button = '<a href="'.$path_to_core.'team/manage.php?id='.$team_id.'">จัดการ</a>';
			}

			$_list .= '
<div class="d-flex"><div class="flex-fill">จำนวนสมาชิก: '.$member_count.' </div>'.$manage_button.'</div>
<div class="d-flex"><div class="flex-fill">ผู้บริหาร: </div></div>
<div class="d-flex team_id'.$team_id.'-member">';
			foreach($team_show1s as $_k=>$_vs){
				$team_user_infos = $team_user->get_info2($team_id,$_vs['user_id']);
				$_list .= '<div style="position:relative;">';
				$_list .= $view->block('list', array('id'=>$_vs['user_id'],'team_id'=>$team_id,'team_user_id'=>$team_user_infos['id']));
				if($team_user_infos['role_id']==1){
					if($vs['super_admin'] == $_vs['user_id']){
						$_list .= '<i style="position:absolute;bottom:5px;right:8px;font-size:0.6em;color:yellow;" class="fas fa-crown"></i>';
					}else{
						$_list .= '<i style="position:absolute;bottom:5px;right:8px;font-size:0.6em;" class="fas fa-crown"></i>';	
					}
				}
				$_list .= '</div>';
			}
			$_list .= '</div>';
// 			$_list .= '
// <div class="d-inline">
// 	<div class="text-center d-inline-block mr-2 mb-2" style="width:30px; height:50px;">
// 		<a href="'.$path_to_core.'user/list.php?team_id='.$team_id.'">
// 			<img class="w-100" src="'.$path_to_core.'system/img/icon/more.png">
// 		</a>
// 	</div>
// </div>';
		}

		$team_show2s = $team_user->get_user_allow1_admin_allow0(array('team_id'=>$team_id));
		if(!empty($team_show2s)){
			$_list .= '<hr class="mb-0"><div class="mr=2">รออนุมัติ: </div><div class="d-flex">';
			foreach($team_show2s  as $_k=>$_vs){
				if($team->check('admin',$team_id)){
					// print_r($_vs);
					$team_user_infos = $team_user->get_info($team_id,$_vs['user_id']);
					$this_user_infos = $user->get_info($_vs['user_id']);
					$_list .= $view->block('list-admin', array(
								'team_user_id'=>$team_user_infos['id'],
								'id'=>$_vs['user_id'],
								'name'=>$this_user_infos['name']
							));
				}else{
					$_list .= $view->block('list', array('id'=>$_vs['user_id']));
				}
			}
			$_list .= '</div>';
		}

		$team_show3s = $team_user->get_user_allow0_admin_allow1(array('team_id'=>$team_id));
		if(!empty($team_show3s)){
			$_list .= '<hr class="mb-0"><div class="mr=2">รอผู้ใช้ยืนยัน: </div><div class="d-flex">';
			foreach($team_show3s as $_k=>$_vs){
				$_list .= $view->block('list', array('id'=>$_vs['user_id']));
			}
			$_list .= '</div>';
		}
		
		$vs['list'] = $_list;
		$vs['active_show']='';
		if($vs['active']!=1){$vs['active_show']='สถานะ: <span class="text-danger">ระงับการใช้งาน</span>';}
		$vs['admin_edit'] = '';
		if($team->check('admin',$team_id)){
			$vs['admin_edit'] = '<a href="edit.php?id='.$team_id.'">แก้ไข</a>';
		}
		$vs['team_image']='';
		if(file_exists($path_to_app.'system/img/team/'.$team_id.'/profile')){
			$vs['team_image']='<img class="mr-2" style="width:50px;" src="'.$path_to_app.'system/img/team/'.$team_id.'/profile">';
		}
		if(!empty($vs['detail'])){
			$vs['detail'] = '<em>'.$vs['detail'].'</em>';
		}
		$vs['team_id'] = $team_id;
		$list .= $view->block('card',$vs);
	}
}

$variables['list'] = $list;

$variables['page-name'] = 'ทีม';
$variables['menu-search'] = '
<div class="mt-2 mr-sm-2 flex-fill">
<form action="'.$path_to_core.'team/search.php" method="get">
	<input type="hidden" name="role_id" value="'.$role_id.'">
	<div class="d-flex">
		<div class="flex-fill mr-2"><input class="bg-blue-light border border-secondary text-white form-control mr-sm-2" type="search" name="search" placeholder="ค้นหา"></div>
		<div class="ml-auto"><button  style="line-height:36px;" class="btn btn-outline-secondary" type="submit">ค้นหา</button></div>
	</div>
</form>
</div>';
echo $view->create($variables);
