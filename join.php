<?php
	
require_once("../system/init.php");

$team = new \pongsit\team\team();
$user = new \pongsit\user\user();
$team_user = new \pongsit\team\user();
$role = new \pongsit\role\role();

if(empty($_GET['team_id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$team_id = $_GET['team_id'];

if(!($team->check('admin',$team_id) || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$team_infos = $team->get_info($team_id);

$variables['team_image'] = '';
if(file_exists('<img class="mr-2" style="width:50px;" src="'.$path_to_app.'system/img/team/'.$team_id.'/profile">')){
	$variables['team_image'] = '<img class="mr-2" style="width:50px;" src="'.$path_to_app.'system/img/team/'.$team_id.'/profile">';
}
$variables['team_name'] = $team_infos['name_show'];
$variables['team_detail'] = '';
if(!empty($team_infos['detail'])){
	$variables['team_detail'] = '<em>'.$team_infos['detail'].'</em>';
}

if($team_user->is_member($team_id,$_SESSION['user']['id'])){
	header('Location: '.$path_to_core.'team/lobby.php?id='.$team_id);
	exit();
}

$variables['team_card'] = $view->block('search',$variables);

echo $view->create($variables);
